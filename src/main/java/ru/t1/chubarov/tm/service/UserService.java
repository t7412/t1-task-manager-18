package ru.t1.chubarov.tm.service;

import ru.t1.chubarov.tm.api.repository.IUserRepository;
import ru.t1.chubarov.tm.api.service.IUserService;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.exception.field.*;
import ru.t1.chubarov.tm.exception.user.UserNotFoundException;
import ru.t1.chubarov.tm.model.User;
import ru.t1.chubarov.tm.util.HashUtil;

import java.util.List;

public final class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User create(final String login, final String password) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(Role.USUAL);
        return userRepository.add(user);
    }

    @Override
    public User create(final String login, final String password, final String email) throws AbstractException {
        if (isEmailExist(email)) throw new EmailEmptyException();
        final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(final String login, final String password, final Role role) throws AbstractException {
        if (role == null) throw new RoleEmptyException();
        final User user = create(login, password);
        user.setRole(role);
        return user;
    }

    @Override
    public boolean isLoginExist(final String login) {
        if (login == null || login.isEmpty()) return false;
        return userRepository.isLoginExist(login);
    }

    @Override
    public boolean isEmailExist(final String email) {
        if (email == null || email.isEmpty()) return false;
        return userRepository.isEmailExist(email);
    }

    @Override
    public User add(final User user) throws AbstractException {
        if (user == null) throw new UserNotFoundException();
        return userRepository.add(user);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final User user = userRepository.findById(id);
        if (user == null) return null;
        return user;
    }

    @Override
    public User findByLogin(final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = userRepository.findByLogin(login);
        if (user == null) return null;
        return user;
    }

    @Override
    public User findByEmail(final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new LoginEmptyException();
        final User user = userRepository.findByEmail(email);
        if (user == null) return null;
        return user;
    }

    @Override
    public User remove(User user) throws AbstractException {
        if (user == null) throw new UserNotFoundException();
        return userRepository.remove(user);
    }

    @Override
    public User removeById(final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final User user = findById(id);
        if (user == null) return null;
        return userRepository.remove(user);
    }

    @Override
    public User removeByLogin(final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = findByLogin(login);
        if (user == null) return null;
        return userRepository.remove(user);
    }

    @Override
    public User removeByEmail(final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        final User user = findByEmail(email);
        if (user == null) return null;
        return userRepository.remove(user);
    }

    @Override
    public User setPassword(final String id, final String password) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public User updateUser(final String id,
                           final String firstName,
                           final String lastName,
                           final String middleName) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

}
