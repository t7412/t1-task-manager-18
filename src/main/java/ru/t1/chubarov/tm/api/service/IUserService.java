package ru.t1.chubarov.tm.api.service;

import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.exception.user.AccessDeniedException;
import ru.t1.chubarov.tm.model.User;

import java.util.List;

public interface IUserService {
    User create(String login, String password) throws AbstractException;

    User create(String login, String password, String email) throws AbstractException;

    User create(String login, String password, Role role) throws AbstractException;

    boolean isLoginExist(String login);

    boolean isEmailExist(String email);

    User add(User user) throws AbstractException;

    List<User> findAll();

    User findById(String id) throws AbstractException;

    User findByLogin(String login) throws AbstractException;

    User findByEmail(String email) throws AbstractException;

    User remove(User user) throws AbstractException;

    User removeById(String id) throws AbstractException;

    User removeByLogin(String login) throws AbstractException;

    User removeByEmail(String email) throws AbstractException;

    User setPassword(String id, String password) throws AbstractException;

    User updateUser(String id,
                    String firstName,
                    String lastName,
                    String middleName) throws AbstractException;

}
