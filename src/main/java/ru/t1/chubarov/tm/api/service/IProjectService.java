package ru.t1.chubarov.tm.api.service;

import ru.t1.chubarov.tm.api.repository.IProjectRepository;
import ru.t1.chubarov.tm.enumerated.Sort;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.chubarov.tm.exception.field.DescriptionEmptyException;
import ru.t1.chubarov.tm.exception.field.IdEmptyException;
import ru.t1.chubarov.tm.exception.field.IndexIncorrectException;
import ru.t1.chubarov.tm.exception.field.NameEmptyException;
import ru.t1.chubarov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IProjectRepository {

    Project create (String name, String description) throws AbstractException;

    List<Project> findAll(Sort sort);

    List<Project> findAll(Comparator comparator);

    void remove(Project project);

    Project findOneById(String id) throws AbstractException;

    Project findOneByIndex(Integer index) throws AbstractException;

    Project updateById(String id, String name, String description) throws AbstractException;

    Project updateByIndex(Integer index, String name, String description) throws AbstractException;

    Project removeById(String id) throws AbstractException;

    Project removeByIndex(Integer index) throws IndexIncorrectException;

    Project changeProjectStatusByIndex(Integer index, Status status) throws AbstractException;

    Project changeProjectStatusById(String id, Status status) throws AbstractException;

}
